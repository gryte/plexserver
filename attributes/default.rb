# Plex Media Server Package
default['plexserver']['package']['name'] = 'plexmediaserver-1.22.0.4145-0af3a4016.x86_64.rpm'
default['plexserver']['version']['url'] = 'https://downloads.plex.tv/plex-media-server-new/1.22.0.4145-0af3a4016/redhat/plexmediaserver-1.22.0.4145-0af3a4016.x86_64.rpm'
